abstract class SessionManagerService {
  void init({
    Function()? inactiveCallBack,
    required Duration inactiveDuration,
  });

  void dispose();

  void overrideInactiveCallBack({required Function() inactiveCallBack});

  void overrideInactiveDuration({
    required Duration inactiveDuration,
  });
}
