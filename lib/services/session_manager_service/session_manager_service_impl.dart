import 'package:flutter/widgets.dart';
import 'package:flutter_app_testing/services/session_manager_service/session_manager_service.dart';

class SessionManagerServiceImpl
    with WidgetsBindingObserver
    implements SessionManagerService {
  late DateTime _backgroundTimestamp;
  late Function() _inactiveCallBack;
  late Duration _inactiveDuration;

  @override
  void init(
      {Function()? inactiveCallBack,
      required Duration inactiveDuration}) {
    _backgroundTimestamp = DateTime.now();
    this._inactiveCallBack = inactiveCallBack ?? _defaultInactiveCallBack;
    this._inactiveDuration = inactiveDuration;
    WidgetsBinding.instance?.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.inactive ||
        state == AppLifecycleState.paused)
      _backgroundTimestamp = DateTime.now();
    else if (state == AppLifecycleState.resumed && _isExceedInactiveDuration())
      _inactiveCallBack();
  }

  @override
  void overrideInactiveCallBack({required Function() inactiveCallBack}) {
    this._inactiveCallBack = inactiveCallBack;
  }

  @override
  void overrideInactiveDuration({
    required Duration inactiveDuration,
  }) {
    this._inactiveDuration = inactiveDuration;
  }

  bool _isExceedInactiveDuration() {
    return DateTime.now().difference(_backgroundTimestamp) >= _inactiveDuration;
  }

  void _defaultInactiveCallBack() {
    print("Inactive Called");
  }
}
