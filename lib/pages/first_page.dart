import 'package:flutter/material.dart';
import 'package:flutter_app_testing/Pages/second_page.dart';
import 'package:flutter_app_testing/services/session_manager_service/session_manager_service.dart';

class FirstPage extends StatelessWidget {
  final SessionManagerService sessionManagerService;

  const FirstPage({Key? key, required this.sessionManagerService})
      : super(key: key);

  void _logout(context) {
    print("Inactive: Logout");
    Navigator.popUntil(context, (route) => false);
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SecondPage(),
        ));
  }

  @override
  Widget build(BuildContext context) {
    sessionManagerService.overrideInactiveCallBack(
        inactiveCallBack: () => _logout(context));
    return Scaffold(
      backgroundColor: Colors.deepPurple,
      body: Center(
          child: Container(
        child: Text("First Page"),
      )),
    );
  }
}
