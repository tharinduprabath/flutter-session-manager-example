import 'package:flutter/material.dart';
import 'package:flutter_app_testing/Pages/first_page.dart';
import 'package:flutter_app_testing/services/session_manager_service/session_manager_service.dart';
import 'package:flutter_app_testing/services/session_manager_service/session_manager_service_impl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late final SessionManagerService sessionManagerService;
  final Duration inactiveDuration = Duration(seconds: 2);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FirstPage(
        sessionManagerService: sessionManagerService,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    sessionManagerService = SessionManagerServiceImpl();
    sessionManagerService.init(inactiveDuration: inactiveDuration);
  }

  @override
  void dispose() {
    sessionManagerService.dispose();
    super.dispose();
  }
}
